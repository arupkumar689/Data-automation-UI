import SideNavbar from '../sideNav/SideNavbar'
import HeaderIndex from '../Header/HeaderIndex'

const UIRenderer = () => {
  return (
  <>
  <SideNavbar/>
  <HeaderIndex/>
  </>
    
  )
}

export default UIRenderer;
