import DataUsageIcon from '@mui/icons-material/DataUsage';
import TableChartIcon from '@mui/icons-material/TableChart';
import GroupsIcon from '@mui/icons-material/Groups';
import AdminPanelSettingsIcon from '@mui/icons-material/AdminPanelSettings';
 const sideIcons=[
    {
        "id":1,
        "Name":"Home",
        "icon": <DataUsageIcon />
    },
    {
        "id":2,
        "Name":"Database Table",
        "icon": <TableChartIcon />
    },
    {
        "id":3,
        "Name":"Users",
        "icon": <GroupsIcon />
    },
    {
        "id":4,
        "Name":"Admin Panel",
        "icon": <AdminPanelSettingsIcon />
    }
 ];
 export default sideIcons;